# inclusivo
With a mission to help get jobs to everyone, Inclusivo is focused upon diversity hiring. We belong, You belong.

Inclusivo is an online platform that makes it easier for people from underprivileged communities to find and get relevant jobs in their respective fields. We connect job seekers and job givers by focusing exclusively on inclusion; we support women, working mothers, the specially challenged, the veterans, LGBTQIA+ folks, and other underrepresented communities.

### Figma Design

https://www.figma.com/file/HM6gDn00Ivq9yhK2WCQptU/Inclusivo?node-id=2%3A243


### Get it on Google Play Store
https://play.google.com/store/apps/details?id=com.dsciiita.inclusivo
