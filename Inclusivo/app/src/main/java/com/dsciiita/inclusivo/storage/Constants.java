package com.dsciiita.inclusivo.storage;

import com.dsciiita.inclusivo.R;

public class Constants {
    public static int FILTER_PAGE_SIZE = 20;

    public static int PLACEHOLDER_IMAGE = R.drawable.placeholder_square;

    public static String FEMALE_URL = "https://imgur.com/jxPu6aF.jpg";
    public static String LGBTQI_URL = "https://imgur.com/MygR67G.png";
    public static String VETERAN_URL = "https://imgur.com/aRVmfM1.jpg";
    public static String WORKING_MOTHER_URL = "https://imgur.com/4zo39Yt.jpg";
    public static String SPECIALLY_ABLED_URL = "https://imgur.com/Nfy0VPy.jpg";

    public static String PUNE_URL = "https://i.imgur.com/m1yOb3C.jpg";
    public static String MUMBAI_URL = "https://i.imgur.com/rJDpW3e.jpg";
    public static String DELHI_URL = "https://i.imgur.com/OYuJUQM.jpg";
    public static String GURGAON_URL = "https://i.imgur.com/YHM5fzS.jpg";
    public static String CHENNAI_URL = "https://i.imgur.com/uhB4F1m.jpg";
    public static String BENGALURU_URL = "https://i.imgur.com/JaBYAv4.jpg";
}
